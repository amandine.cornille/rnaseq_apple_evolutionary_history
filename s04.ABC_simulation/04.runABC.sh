#!/bin/bash

#SBATCH -J job
#SBATCH -o job.out
#SBATCH -e job.err

module purge

bash RUN_before_launching_simulations.sh
ABCtoolbox SET.input task=simulate > /dev/null
