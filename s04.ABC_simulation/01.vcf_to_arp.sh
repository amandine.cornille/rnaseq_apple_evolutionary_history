#!/bin/bash
# By Chen Xilong
# Create Date: 1/14/2020 11:45
# Contact: chen_xilong@outlook.com

vcftools \
  --vcf ../../../Dataset/01_Original_dataset/LD_pruning.synonymous.name.vcf \
  --keep list.txt --recode-INFO-all \
  --out SET --recode
