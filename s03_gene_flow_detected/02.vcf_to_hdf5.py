#!/usr/bin/python
import ipyrad.analysis as ipa
import pandas as pd

converter = ipa.vcf_to_hdf5(
    name="Final",
    data="../Dataset/03_Final_dataset/Final.recode.vcf",
    ld_block_size=200,

)

# run the converter
converter.run()
