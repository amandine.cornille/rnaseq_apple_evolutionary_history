#!/usr/bin/python

import ipyrad.analysis as ipa
import toytree
import toyplot

print('ipyrad', ipa.__version__)
print('toytree', toytree.__version__)
! treemix --version | grep 'TreeMix v. '

# the path to your HDF5 formatted snps file
data = "./analysis-vcf2hdf5/Final.snps.hdf5"

imap = {
    "BAC_HUP": ["C105_Wc_China", "C108_Wc_China", "C113_Wc_China", "W-12", "W-13", "W-14", "W-15", "W-16", "W-17", "W-18", "W-19", "W-22", "W-26", "W-31", "W-41L", "W-42L"],
    "NorthA": ["A24_W_USA", "A29_W_USA", "A30_W_USA", "W-43L", "W-44"],
    "CULT_ROOT_MO":["R1_D_Poland", "R10_D_USA", "R11_D_USA", "R12_D_USA", "R2_D_CzeCh", "R3_D_UK", "R5_D_Canada", "R6_D_Canada", "R-69", "R7_D_UK", "R-70", "R-71", "R-72", "R-75", "R-77", "R8_D_USA"],
    "CULT_DOM":["A00_D_USA", "A01_D_Israel", "A02_D_Russia", "A03_D_France", "A04_D_UK", "A05_D_UK", "A06_D_USA", "A07_D_USA", "A08_D_Japan", "A09_D_NewZealand", "A10_D_Australia", "A11_D_USA", "A12_D_France", "A13_D_USA", "A14_D_USA", "A15_D_Russia", "A16_D_USA", "A17_D_USA", "A18_D_Canada", "A19_D_Russia", "A20_D_UK", "A21_D_USA", "A22_D_UK", "A23_D_Germany", "D-49", "D-62"],
    "SIE": ["A31_W_Russia", "A35_S_Kazakhstan", "A36_S_Kazakhstan", "A37_S_Kazakhstan", "A38_S_Kazakhstan", "A39_S_Kazakhstan", "A41_S_Kazakhstan", "A42_S_Kazakhstan", "A43_S_Kazakhstan", "A44_S_Kazakhstan", "A45_S_Kazakhstan", "A47_S_Kazakhstan", "A48_S_Kazakhstan", "C74_S_Xinjiang", "C77_S_Xinjiang", "C78_S_Xinjiang", "C80_S_Xinjiang", "C81_S_Xinjiang", "C85_S_Xinjiang", "C86_S_Xinjiang", "C87_S_Xinjiang", "C88_S_Xinjiang", "C89_S_Xinjiang", "C90_S_Xinjiang", "C93_S_Xinjiang", "C94_S_Xinjiang", "W-01", "W-07", "W-08"],
    "SYL": ["A50_St_USA", "A51_St_Germany", "A52_St_Germany", "A53_St_Germany", "A54_St_Germany", "A55_St_Yugoslavia", "A56_St_UK" ]
}

# minimum % of samples that must be present in each SNP from each group
minmap = {i: 0.5 for i in imap}

tmx = ipa.treemix(
    data=data,
    imap=imap,
    minmap=minmap,
    seed=1234,
    root="NorthA",
)
tests = {}
nadmix = [0, 1, 2, 3, 4, 5]
# iterate over n admixture edges and store results in a dictionary
for adm in nadmix:
    tmx.params.m = adm
    tmx.run()
    tests[adm] = tmx.results.llik
toyplot.plot(
    nadmix,
    [tests[i] for i in nadmix],
    width=350,
    height=275,
    stroke_width=3,
    xlabel="n admixture edges",
    ylabel="ln(likelihood)",
);

tmx = ipa.treemix(
    data=data,
    imap=imap,
    minmap=minmap,
    root="NorthA",
    seed=1234,
    m=3,
)
tmx.run()
tmx.draw_tree()
tmx.draw_cov()


# group individuals into populations
imap = {
    "BAC_HUP": ["C105_Wc_China", "C108_Wc_China", "C113_Wc_China", "W-12", "W-13", "W-14", "W-15", "W-16", "W-17", "W-18", "W-19", "W-22", "W-26", "W-31", "W-41L", "W-42L"],
    "NorthA": ["A24_W_USA", "A29_W_USA", "A30_W_USA", "W-43L", "W-44"],
    "CULT_ROOT_CN":["C100_Wc_China", "C104_Wc_China", "D-50", "D-56", "D-57", "D-58", "D-59"],
    "CULT_CHN":["C97_Wc_China", "C98_Wc_China", "C99_Wc_China", "D-46", "D-48"],
    "ROOT_MO":["R1_D_Poland", "R10_D_USA", "R11_D_USA", "R12_D_USA", "R2_D_CzeCh", "R3_D_UK", "R5_D_Canada", "R6_D_Canada", "R-69", "R7_D_UK", "R-70", "R-71", "R-72", "R-75", "R-77", "R8_D_USA"],
    "CULT_DOM":["A00_D_USA", "A01_D_Israel", "A02_D_Russia", "A03_D_France", "A04_D_UK", "A05_D_UK", "A06_D_USA", "A07_D_USA", "A08_D_Japan", "A09_D_NewZealand", "A10_D_Australia", "A11_D_USA", "A12_D_France", "A13_D_USA", "A14_D_USA", "A15_D_Russia", "A16_D_USA", "A17_D_USA", "A18_D_Canada", "A19_D_Russia", "A20_D_UK", "A21_D_USA", "A22_D_UK", "A23_D_Germany", "D-49", "D-62"],
    "SIE": ["A31_W_Russia", "A35_S_Kazakhstan", "A36_S_Kazakhstan", "A37_S_Kazakhstan", "A38_S_Kazakhstan", "A39_S_Kazakhstan", "A41_S_Kazakhstan", "A42_S_Kazakhstan", "A43_S_Kazakhstan", "A44_S_Kazakhstan", "A45_S_Kazakhstan", "A47_S_Kazakhstan", "A48_S_Kazakhstan", "C74_S_Xinjiang", "C77_S_Xinjiang", "C78_S_Xinjiang", "C80_S_Xinjiang", "C81_S_Xinjiang", "C85_S_Xinjiang", "C86_S_Xinjiang", "C87_S_Xinjiang", "C88_S_Xinjiang", "C89_S_Xinjiang", "C90_S_Xinjiang", "C93_S_Xinjiang", "C94_S_Xinjiang", "W-01", "W-07", "W-08"],
    "SYL": ["A50_St_USA", "A51_St_Germany", "A52_St_Germany", "A53_St_Germany", "A54_St_Germany", "A55_St_Yugoslavia", "A56_St_UK" ]
}

# minimum % of samples that must be present in each SNP from each group
minmap = {i: 0.5 for i in imap}


tmx = ipa.treemix(
    data=data,
    imap=imap,
    minmap=minmap,
    seed=1234,
    root="NorthA",
)
tests = {}
nadmix = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
# iterate over n admixture edges and store results in a dictionary
for adm in nadmix:
    tmx.params.m = adm
    tmx.run()
    tests[adm] = tmx.results.llik
toyplot.plot(
    nadmix,
    [tests[i] for i in nadmix],
    width=350,
    height=275,
    stroke_width=3,
    xlabel="n admixture edges",
    ylabel="ln(likelihood)",
);

tmx = ipa.treemix(
    data=data,
    imap=imap,
    minmap=minmap,
    root="NorthA",
    seed=1234,
    m=5,
)
tmx.run()
tmx.draw_tree()
tmx.draw_cov()
