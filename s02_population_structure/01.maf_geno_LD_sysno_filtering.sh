#!/bin/bash
# By Chen Xilong


# soft filtering
bcftools filter -e 'F_MISSING > 0.1 || MAF <= 0.01' -O z -o RNA-seq_WGS_Combine.vcf.gz Total_SNPs.vcf.recode.vcf.gz
plink --vcf Total_SNPs.vcf.recode.vcf.gz --make-bed --const-fid --out b.Final --set-missing-var-ids \@\:# --keep-allele-order --allow-extra-chr
# LD prune
plink --bfile b.filter --indep-pairwise 1 kb 1 0.2 --out plink --allow-extra-chr
plink --bfile b.filter --extract plink.prune.in --make-bed --out b.LD_pruning --keep-allele-order --allow-extra-chr
plink --bfile b.LD_pruning --recode vcf --out LD_pruning_vcf --keep-allele-order --allow-extra-chr

# filter non-synonymous sites
java -Xmx4g -jar /work/chenxilong/biosoft/snpEff/snpEff.jar -csvStats Final_vcf.stat GDDH1-1 $data_dir/Final.vcf > Final.ann.vcf
perl filter_synonymous_variant.pl LD_pruning_vcf.ann.vcf LD_pruning_vcf.synonymous.vcf

# filter the clone
plink --vcf LD_pruning_vcf.synonymous.vcf --make-bed --const-fid --out b.LD_pruning_vcf.synonymous --set-missing-var-ids \@\:# --keep-allele-order

vcftools \
  --vcf ../01_Original_dataset/LD_pruning.synonymous.vcf \
  --keep kinship_keep.txt --recode-INFO-all \
  --out after_kinship --recode &&\

bgzip -c after_kinship.recode.vcf > after_kinship.recode.vcf.gz && tabix -p vcf after_kinship.recode.vcf.gz
