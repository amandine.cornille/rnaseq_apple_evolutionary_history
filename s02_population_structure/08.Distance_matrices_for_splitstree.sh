#!/bin/bash
# By Xilong CHEN
# Create Date: 4/6/2020 13:02
# Contact: chen_xilong@outlook.com

plink --vcf ../Final.recode.vcf --recode --make-bed --const-fid --out ./Final_dataset --set-missing-var-ids \@\:# --keep-allele-order
plink --bfile Final_dataset --distance 1-ibs square
