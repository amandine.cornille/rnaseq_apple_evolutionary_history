#!/usr/bin/perl
use warnings;
use strict;
use Data::Dumper;
use File::Basename;

# By Chen Xilong
# Create Date: 2/6/2020 15:29
# Contact: chen_xilong@outlook.com

my $re_folder = "./";

my $outputdir = "./input_for_poph";

for my $k_f ( 2 .. 9 ) {
    my @Qfiles
        = glob
        "$re_folder/K=$k_f/M*Cluster*/CLUMPP.files/ClumppIndFile.output";

    # print Dumper \@Qfiles;
    for my $f (@Qfiles) {
        print "$f\n\n";
        $f =~ /=\d+\/(.+?Cluster\d*)/;
        my $m = $1;
        open my $in, "<", "$f";
        print "K.$k_f.$m.Q\n";
        open my $ou, ">", "$outputdir/K.$k_f.$m.Q";
        while (<$in>) {
            $_ =~ s/^.+: //;
            print $ou $_;
        }
    }
}

