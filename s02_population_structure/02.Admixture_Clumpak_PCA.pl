#!/usr/bin/perl
use warnings;
use strict;
use Data::Dumper;
use File::Basename;
use File::Copy;

# By Chen Xilong
# Create Date: 3/11/2018 12:51
# Contact: chen_xilong@outlook.com
# Add admixture to multipule run for CLUMPAK
my $pl_dir = "/work/chenxilong/biosoft/my_pl";

my $data_dir = $ARGV[0];
my $in       = $ARGV[1];
my $thread   = $ARGV[2];

my $usage
    = "usage: perl $0 <data_dir>(data dir have vcf file) <resultdir description>(like date) <max threads number>(for some phylo analysis)\nThis script only for mdom vcf!!";
die $usage if $#ARGV != 2;

$in .= "_pop";
my $result_dir    = "../$in\_result";
my $shell_dir     = "../$in\_shell";
my $out_error_dir = "../$in\_out_error";

`
if [ ! -e $result_dir ]
    then mkdir $result_dir
fi
if [ ! -e $out_error_dir ]
    then mkdir $out_error_dir
fi
if [ ! -e $shell_dir ]
    then mkdir $shell_dir
fi
`;
my $vcf_to_bed_file = "$shell_dir/$in.vcf_to_bed.sh";
my $PCA_file                   = "$shell_dir/$in.PCA.sh";
my $admix_cv_file              = "$shell_dir/$in.admix_cv.sh";
my $admix_pre_file             = "$shell_dir/$in.admix_pre.sh";
my $stat_zip_admix_file        = "$shell_dir/$in.stat_zip_admix.sh";

my $run_file = "$shell_dir/00.run.sh";

########### SNP vcf to plink #########################
open my $fh_vcf_to_bed, ">", $vcf_to_bed_file;
binmode $fh_vcf_to_bed;
print $fh_vcf_to_bed "
plink --vcf $data_dir/after_kinship.recode.vcf.gz --make-bed --const-fid --out $result_dir/b.after_kinship --set-missing-var-ids \@\:# --keep-allele-order --allow-extra-chr && \\
";
close $fh_vcf_to_bed;
###################################################################################


################# PCA ################
open my $fh_PCA, ">", $PCA_file;
binmode $fh_PCA;
print $fh_PCA "
if [ ! -e $result_dir/PCA ]
    then mkdir $result_dir/PCA
fi
sleep 1
plink --bfile $result_dir/b.after_kinship --pca --out $result_dir/PCA/pca --allow-extra-chr";
close $fh_PCA;

############## Structure useing admixture  #####################
open my $fh_admix_cv, ">", $admix_cv_file;
binmode $fh_admix_cv;
print $fh_admix_cv "
if [ ! -e $result_dir/admix_cv ]
    then mkdir $result_dir/admix_cv
fi
sleep 1

cp $result_dir/b.after_kinship.* $result_dir/admix_cv/

while [ ! -e $result_dir/admix_cv/b.after_kinship.bed ]
do
    sleep 1
done

cd $result_dir/admix_cv/
# here the dir is $result_dir/admix_cv/
for K in 1 2 3 4 5 6 7 8 9 10
do
    admixture --cv b.after_kinship.bed \$K -j$thread | tee log\${K}.out
done
";
close $fh_admix_cv;

open my $fh_admix_pre, ">", $admix_pre_file;
binmode $fh_admix_pre;
print $fh_admix_pre "
if [ ! -e $result_dir/admix_run ]
    then mkdir $result_dir/admix_run
fi
sleep 1
cp $result_dir/b.after_kinship.* $result_dir/admix_run/

while [ ! -e $result_dir/admix_run/b.after_kinship.bed ]
do
    sleep 1
done
";
close $fh_admix_pre;

for ( 1 .. 20 ) {
    open my $ou, ">", "$shell_dir/$in.admix_run$_.sh";
    binmode $ou;
    print $ou "
if [ ! -e $result_dir/admix_run/run$_ ]
    then mkdir $result_dir/admix_run/run$_
fi
sleep 1
cd $result_dir/admix_run/run$_
for K in 2 3 4 5 6 7 8 9 10
do
    admixture -s time ../b.after_kinship.bed \$K -j$thread && \\
    cp b.after_kinship.\$K.Q ../mdom.datafile_res_\$K\\_run$_\_admixture
done";
    close $ou;
}

################ stat data for admix and mk zip for web CLUMPAK ######################
open my $fh_stat_zip_admix, ">", $stat_zip_admix_file;
binmode $fh_stat_zip_admix;
print $fh_stat_zip_admix "
cd $result_dir/admix_cv
grep -h CV log*.out > admix_cv.stat

cd ../admix_run

for K in 2 3 4 5 6 7 8 9 10
do
    zip -q -r K\$K\\_data.zip mdom.datafile_res_\$K*
done
";
close $fh_stat_zip_admix;

########################################################

############### write shell for this pipeline ###########################
open my $fh_run, ">", $run_file;
binmode $fh_run;


# PCA and admix qsub
print $fh_run "
qsub -P s2pnh -q s2pnh.q -e $out_error_dir -o $out_error_dir -cwd -l vf=2g,p=1 $in.vcf_to_bed.sh

qsub -P s2pnh -hold_jid $in.anno_snp.sh -q s2pnh.q -e $out_error_dir -o $out_error_dir -cwd -l vf=1g,p=1 $in.PCA.sh

qsub -P s2pnh -hold_jid $in.anno_snp.sh -q s2pnh.q -e $out_error_dir -o $out_error_dir -cwd -l vf=1g,p=1 $in.admix_pre.sh
qsub -P s2pnh -hold_jid $in.admix_pre.sh -q s2pnh.q -e $out_error_dir -o $out_error_dir -cwd -l vf=1g,p=$thread $in.admix_cv.sh";
for ( 1 .. 20 ) {
    print $fh_run "
qsub -P s2pnh -hold_jid $in.admix_pre.sh -q s2pnh.q -e $out_error_dir -o $out_error_dir -cwd -l vf=1g,p=$thread $in.admix_run$_.sh";
}
print $fh_run "
# qsub -P s2pnh -hold_jid $in.admix_run20.sh -q s2pnh.q -e $out_error_dir -o $out_error_dir -cwd -l vf=1g,p=1 $in.stat_zip_admix.sh";

close $fh_run;
