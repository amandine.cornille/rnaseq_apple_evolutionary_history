#!/bin/bash

conda activate stacks-2.52
populations -V after_kinship.recode.vcf.vcf -O ./output/ -M group.txt --smooth -t 8
conda deactivate