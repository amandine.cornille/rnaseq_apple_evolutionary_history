#!/bin/bash
# By Chen Xilong

/work/software/java \
	-Djava.io.tmpdir=/work/xchen/gatk.snp/result/A00_D_USA/tmp \
	-jar /work/software/picard/AddOrReplaceReadGroups.jar \
		I=A00_D_USA.sam O=A00_D_USA.addRG.bam \
		RGID=A00_D_USA \
		RGLB=A00_D_USA.lib \
		RGPL=illumina \
		RGPU=machine RGSM=A00_D_USA VALIDATION_STRINGENCY=SILENT && \

samtools sort -@ 4 -m 4G -O bam -o A00_D_USA.sorted.bam A00_D_USA.rg.bam && echo "sort bam done!"

/work/software/java \
	-Djava.io.tmpdir=/work/xchen/gatk.snp/result/A00_D_USA/tmp \
	-jar /work/software/picard/MarkDuplicates.jar \
    -I A00_D_USA.sorted.bam \
-O A00_D_USA.sorted.markdu.bam \
-M A00_D_USA.sorted.markdu_metrics.txt \
&& echo "Mark dupilcate done!"

rm -f A00_D_USA.bam
rm -f A00_D_USA.rg.bam
rm -f A00_D_USA.sorted.bam

samtools index A00_D_USA.sorted.markdu.bam \
&& echo "bam index done!"


/work/software/java1.8 \
	-Djava.io.tmpdir=/work/xchen/gatk.snp/result/D-46A/tmp \
	-jar /work/software/GenomeAnalysisTK.jar \
    -T HaplotypeCaller
    -R ref.fasta \
    --emit-ref-confidence GVCF \
    -I A00_D_USA.sorted.markdu.bam \
    -O A00_D_USA.g.vcf.gz \
&& echo "call gvcf done!"
