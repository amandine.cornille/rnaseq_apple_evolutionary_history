#!/bin/bash
# By Chen Xilong


/work/software/java1.8 \
	-Djava.io.tmpdir=/work/xchen/gatk.snp/result/D-46A/tmp \
	-jar /work/software/picard/AddOrReplaceReadGroups.jar \
		I=D-46A.sam O=D-46A.addRG.bam \
		RGID=D-46A \
		RGLB=D-46A.lib \
		RGPL=illumina \
		RGPU=machine RGSM=D-46A VALIDATION_STRINGENCY=SILENT && \
rm D-46A.sam

[ -e D-46A.addRG.bam ] && \
/work/software/java1.8 \
	-Djava.io.tmpdir=/work/xchen/gatk.snp/result/D-46A/tmp \
	-jar /work/software/picard/ReorderSam.jar \
		I=D-46A.addRG.bam O=D-46A.addRG.Reorder.bam \
		R=ref.fasta \
		VALIDATION_STRINGENCY=SILENT && \
rm D-46A.addRG.bam

[ -e D-46A.addRG.Reorder.bam ] && \
/work/software/java1.8 \
	-Djava.io.tmpdir=/work/xchen/gatk.snp/result/D-46A/tmp \
	-jar /work/software/picard/SortSam.jar \
		I=D-46A.addRG.Reorder.bam \
		O=D-46A.addRG.Reorder.Sort.bam \
		SO=coordinate \
		VALIDATION_STRINGENCY=SILENT && \
rm D-46A.addRG.Reorder.bam

[ -e D-46A.addRG.Reorder.Sort.bam ] && \
/work/software/java1.8 \
	-Djava.io.tmpdir=/work/xchen/gatk.snp/result/D-46A/tmp \
	-jar /work/software/picard/MarkDuplicates.jar \
		REMOVE_DUPLICATES=false \
		I=D-46A.addRG.Reorder.Sort.bam \
		O=D-46A.addRG.Reorder.Sort.markDup.bam \
		METRICS_FILE=D-46A.addRG.Reorder.Sort.markDup.metrics \
		VALIDATION_STRINGENCY=SILENT && \
rm D-46A.addRG.Reorder.Sort.bam

[ -e D-46A.addRG.Reorder.Sort.markDup.bam.bai ] || \
/work/software/samtools index \
	D-46A.addRG.Reorder.Sort.markDup.bam

[ -e D-46A.addRG.Reorder.Sort.markDup.splitN.bam ] || \
/work/software/java1.8 \
	-Djava.io.tmpdir=/work/xchen/gatk.snp/result/D-46A/tmp \
	-jar /work/software/GenomeAnalysisTK.jar \
	-T SplitNCigarReads \
	-R ref.fasta \
	-I D-46A.addRG.Reorder.Sort.markDup.bam \
	-o D-46A.addRG.Reorder.Sort.markDup.splitN.bam \
	-rf ReassignOneMappingQuality \
	-RMQF 255 \
	-RMQT 60 \
	-U ALLOW_N_CIGAR_READS && \
rm D-46A.addRG.Reorder.Sort.markDup.bam

[ -e D-46A.addRG.Reorder.Sort.markDup.splitN.bam.bai ] || \
/work/software/samtools index \
	D-46A.addRG.Reorder.Sort.markDup.splitN.bam

/work/software/java1.8 \
	-Xmx10G -Djava.io.tmpdir=/work/xchen/gatk.snp/result/D-46A/tmp \
	-jar /work/software/GenomeAnalysisTK.jar \
		-T HaplotypeCaller \
		-R ref.fasta \
        --emit-ref-confidence GVCF \
		-I D-46A.addRG.Reorder.Sort.markDup.splitN.bam \
		-dontUseSoftClippedBases \
		-stand_call_conf 20.0 \
		-stand_emit_conf 20.0 \
		-o D-46A.gatk.g.vcf




