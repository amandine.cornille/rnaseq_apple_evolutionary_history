#!/bin/bash
# By Chen Xilong

hisat2 \
	--phred64 \
	--sensitive \
	--no-discordant \
	--no-mixed \
	-I 1 \
	-X 1000 \
	-x ref.fasta \
	-1 D-46A_1.fq.gz \
	-2 D-46A_2.fq.gz \
	-S D-46A.sam
    