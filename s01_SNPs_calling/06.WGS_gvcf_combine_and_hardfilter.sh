#!/bin/bash
# By Chen Xilong

/work/software/java1.8 \
 	-jar /work/software/GenomeAnalysisTK.jar \
    -T CombineGVCFs \
    -R ref.fasta \
    -V g.vcf.list \
    -O combine.g.vcf.gz && \
echo "combine gvcfs done!"

/work/software/java1.8 \
 	-jar /work/software/GenomeAnalysisTK.jar \
    -T GenotypeGVCFs \
    -R ref.fasta \
    -V combine.g.vcf.gz \
    -O combine.vcf.gz && \
echo "Genotype gvcf to vcf done!"

/work/software/java1.8 \
 	-jar /work/software/GenomeAnalysisTK.jar \
    -T VariantFiltration \
    -R ref.fasta \
    -V combine.vcf.gz \
    --filter-expression "!vc.hasAttribute('DP')" \
    --filter-name "noCoverage" \
    --filter-expression "vc.hasAttribute('DP') && DP < 5" \
    --filter-name "MinCov" \
    -filter-expression "(vc.isSNP() && (vc.hasAttribute('ReadPosRankSum') && ReadPosRankSum < -8.0)) || ((vc.isIndel() || vc.isMixed()) && (vc.hasAttribute('ReadPosRankSum') && ReadPosRankSum < -20.0)) || (vc.hasAttribute('QD') && QD < 2.0) " \
    --filter-name "badSeq" \
    --filter-expression "(vc.isSNP() && ((vc.hasAttribute('FS') && FS > 60.0) || (vc.hasAttribute('SOR') &&  SOR > 3.0))) || ((vc.isIndel() || vc.isMixed()) && ((vc.hasAttribute('FS') && FS > 200.0) || (vc.hasAttribute('SOR') &&  SOR > 10.0)))" \
    --filter-name "badStrand" \
    --filter-expression "vc.isSNP() && ((vc.hasAttribute('MQ') && MQ < 40.0) || (vc.hasAttribute('MQRankSum') && MQRankSum < -12.5))" \
    --filter-name "badMap" \
    -O filted.vcf.gz && \
echo "hard filter done!"

/work/software/java1.8 \
 	-jar /work/software/GenomeAnalysisTK.jar \
    -T SelectVariants \
-R ref.fasta \
-V filted.vcf.gz \
--exclude-filtered true \
-O filted_passed_sites.vcf.gz && \
echo "generate the passed vcf done!"

/work/software/java1.8 \
 	-jar /work/software/GenomeAnalysisTK.jar \
    -T SelectVariants \
    -R ref.fasta \
    -V filted_passed_sites.vcf.gz \
    --select-type-to-include SNP \
    -O filted_passed_WGS_snps.vcf.gz && \
echo "get the final snps file."
