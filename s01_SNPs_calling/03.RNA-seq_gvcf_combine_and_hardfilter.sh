#!/bin/bash
# By Chen Xilong

/work/software/java1.8 \
 	-jar /work/software/GenomeAnalysisTK.jar \
    -T CombineGVCFs \
    -R ref.fasta \
    -V g.vcf.list \
    -O combine.g.vcf.gz && \
echo "combine gvcfs done!"

/work/software/java1.8 \
 	-jar /work/software/GenomeAnalysisTK.jar \
    -T GenotypeGVCFs \
    -R ref.fasta \
    -V combine.g.vcf.gz \
    -O combine.vcf.gz && \
echo "Genotype gvcf to vcf done!"


/work/software/java1.8 \
 	-jar /work/software/GenomeAnalysisTK.jar \
     -T VariantFiltration 
     -R ref.fasta 
     -V ombine.vcf.gz
     -window 35
     -cluster 3 
     -filterName FS -filter "FS > 30.0" 
     -filterName QD -filter "QD < 2.0" 
     -o filted.vcf.gz

/work/software/java1.8 \
 	-jar /work/software/GenomeAnalysisTK.jar \
    -T SelectVariants \
-R ref.fasta \
-V filted.vcf.gz \
--exclude-filtered true \
-O filted_passed_sites.vcf.gz && \
echo "generate the passed vcf done!"

/work/software/java1.8 \
 	-jar /work/software/GenomeAnalysisTK.jar \
    -T SelectVariants \
    -R ref.fasta \
    -V filted_passed_sites.vcf.gz \
    --select-type-to-include SNP \
    -O filted_passed_RNA-seq_snps.vcf.gz && \
echo "get the final snps file."